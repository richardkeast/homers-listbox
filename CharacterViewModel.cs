﻿using HomersListBox.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace HomersListBox
{
    public class CharacterViewModel : INotifyPropertyChanged
    {
        private string _btext;

        public int SeatSize { get; set; }
        public ObservableCollection<Character> Characters { get; set; }
        public ObservableCollection<SeatViewModel> Seats { get; set; }

        public ICommand Button_Click { get; private set; }

        public CharacterViewModel()
        {
            SeatSize = 30;
            BText = "Hello";
            Button_Click = new RelayCommand(AddChar, null);
            addChars();
            addSeats();
        }
		
		public void AddChar(object p)
		{
			var charLisa = new Character() {First="Lisa", Last="Simpson", Age=8, Gender=Gender.Female, Image="images/lisa.png", X=(3 * SeatSize), Y=0, Width=SeatSize, Height=SeatSize};
            Characters.Add(charLisa);
            OnPropertyChanged("Characters");
            var seatmuch = new SeatViewModel() { SeatType = eSeatTypes.Standard, SeatState = eSeatStates.Selected, SeatText = "4", X = (4 * SeatSize), Y = (0 * SeatSize), Width=SeatSize, Height=SeatSize};
            Seats.Add(seatmuch);
            OnPropertyChanged("Seats");
           // addChars();
            //BText = "Hello!";
		}

        private void addChars()
        {
            Characters = new ObservableCollection<Character>()
            {
                new Character() {First="Bart" + DateTime.Now.Second.ToString(), Last="Simpson", Age=10, Gender=Gender.Male, Image="images/bart.png", X=0, Y=0, Width=SeatSize, Height=SeatSize},
                new Character() {First="Homer", Last="Simpson", Age=38, Gender=Gender.Male, Image="images/homer.png", X=(1 * SeatSize), Y=0, Width=SeatSize, Height=SeatSize},
                new Character() {First="Lisa", Last="Simpson", Age=8, Gender=Gender.Female, Image="images/lisa.png", X=(2 * SeatSize), Y=0, Width=SeatSize, Height=SeatSize}
            };
            OnPropertyChanged("Characters");
        }

        private void addSeats()
        {
            Seats = new ObservableCollection<SeatViewModel>()
            {
                new SeatViewModel() {X=(1 * SeatSize), Y=(0 * SeatSize), Width=SeatSize, Height=SeatSize, SeatText="1", SeatType=eSeatTypes.Standard, SeatState=eSeatStates.Empty },
                new SeatViewModel() {X=(2 * SeatSize), Y=(0 * SeatSize), Width=SeatSize, Height=SeatSize, SeatText="2", SeatType=eSeatTypes.Standard, SeatState=eSeatStates.Selected },
                new SeatViewModel() {X=(3 * SeatSize), Y=(0 * SeatSize), Width=SeatSize, Height=SeatSize, SeatText="3", SeatType=eSeatTypes.Standard, SeatState=eSeatStates.Empty }
            };
            OnPropertyChanged("Seats");
        }

        public string BText
        {
            get { return _btext; }
            private set
            {
                _btext = value;
                OnPropertyChanged("BText");
            }
        }
        
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomersListBox.ViewModel
{
    public class ScreenViewModel : INotifyPropertyChanged
    {
        public int SeatSize { get; set; }
        public ObservableCollection<SeatViewModel> Seats { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        public ScreenViewModel()
        {
            SeatSize = 30;
            Seats = new ObservableCollection<SeatViewModel>();
            for (int x = 1; x < 20; x++)
            {
                for (int y = 0; y < 10; y++)
                {
                    Seats.Add(new SeatViewModel() { X = (x * SeatSize), Y = (y * SeatSize), Width = SeatSize, Height = SeatSize, SeatText = x.ToString(), SeatType = eSeatTypes.Standard, SeatState = eSeatStates.Empty });
                }
            }
            OnPropertyChanged("Seats");
        }
    }
}

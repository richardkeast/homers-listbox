﻿namespace HomersListBox.ViewModel
{
    public enum eSeatStates
    {
        Broken,
        Empty,
        Preselected,
        Selected,
        Reserved,
        Booking,
        TelephoneBooking,
        OnlineBooking
    }
}

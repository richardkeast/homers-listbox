﻿namespace HomersListBox.ViewModel
{
    public enum eSeatTypes
    {
        Standard,
        House,
        Double,
        Wheelchair,
        Box,
        Standing
    }
}
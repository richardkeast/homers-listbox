﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomersListBox;

namespace HomersListBox.ViewModel
{
    public class SeatViewModel : INotifyPropertyChanged
    {
        public eSeatTypes SeatType { get; set; }
        public eSeatStates SeatState { get; set; }
        public string SeatText { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }

        //public List<SeatState>

        public SeatViewModel()
        {

        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}

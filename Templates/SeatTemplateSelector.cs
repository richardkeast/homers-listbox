﻿using HomersListBox.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace HomersListBox.Templates
{
    public class SeatTemplateSelector : DataTemplateSelector
    {
        private DataTemplate _standardSeatTemplate = null;
        public DataTemplate StandardSeatTemplate
        {
            get { return _standardSeatTemplate; }
            set { _standardSeatTemplate = value; }
        }

        private DataTemplate _wheelchairTemplate = null;
        public DataTemplate WheelchairTemplate
        {
            get { return _wheelchairTemplate; }
            set { _wheelchairTemplate = value; }
        }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            DataTemplate template = base.SelectTemplate(item, container);
            if (item is SeatViewModel)
            {
                var seat = (item as SeatViewModel);
                switch (seat.SeatType)
                {
                    case eSeatTypes.Standard:
                        template = StandardSeatTemplate;
                        break;
                    case eSeatTypes.Wheelchair:
                        template = WheelchairTemplate;
                        break;
                }
            }
            return template;
        }
    }
}

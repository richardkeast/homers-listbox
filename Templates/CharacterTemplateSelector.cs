﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace HomersListBox.Templates
{
    public class CharacterTemplateSelector : DataTemplateSelector
    {
        private DataTemplate _childTemplate = null;
        public DataTemplate ChildTemplate
        {
            get { return _childTemplate; }
            set { _childTemplate = value; }
        }

        private DataTemplate _adultTemplate = null;
        public DataTemplate AdultTemplate
        {
            get { return _adultTemplate; }
            set { _adultTemplate = value; }
        }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if (item is Character)
            {
                return (item as Character).Age >= 21 ? _adultTemplate : _childTemplate;
            }
            return base.SelectTemplate(item, container);
        }
    }
}

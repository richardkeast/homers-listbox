﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HomersListBox
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class IconButton : UserControl
    {
		public IconButton()
        {
			InitializeComponent();
        }

        public static readonly DependencyProperty ButtonTextProperty =
            DependencyProperty.Register("ButtonText", typeof(string), typeof(IconButton));

		public string ButtonText 
		{
            get { return (string)GetValue(ButtonTextProperty); } 
			set { SetValue(ButtonTextProperty, value); } 
		} 

		public ImageSource ButtonIcon 
		{ 
			get { return buttonIcon.Source; } 
			set { buttonIcon.Source = value; } 
		} 
		
		public Brush ButtonBackground
		{
			get { return buttonBorder.Background; }
			set
			{ 
				buttonBorder.Background = value; 
			}
		}
		
		public Brush ButtonBorder
		{
			get { return buttonBorder.BorderBrush; }
			set
			{ 
				buttonBorder.BorderBrush = value; 
			}
		}
    }
}

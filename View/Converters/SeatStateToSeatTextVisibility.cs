﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using HomersListBox.ViewModel;
using System.Windows;

namespace HomersListBox.View.Converters
{
    public class SeatStateToSeatTextVisibility : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                throw new ArgumentNullException("value");
            }

            if (!(value is eSeatStates))
            {
                throw new ArgumentException("Value must be an eSeatStates", "value");
            }

            var result = Visibility.Hidden;
            eSeatStates seatState = (eSeatStates)value;

            if (seatState == eSeatStates.Empty)
            {
                result = Visibility.Visible;
            }

            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}

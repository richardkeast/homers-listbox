﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HomersListBox
{
	/// <summary>
	/// Interaction logic for Button.xaml
	/// </summary>
	public partial class Button : UserControl
	{
		public Button()
		{
			this.InitializeComponent();
		}
		
		public static readonly DependencyProperty ButtonTextProperty =
            DependencyProperty.Register("ButtonText", typeof(string), typeof(Button));

		public string ButtonText 
		{
            get { return (string)GetValue(ButtonTextProperty); } 
			set { SetValue(ButtonTextProperty, value); } 
		} 

        public double ButtonFontSize
        {
            get { return buttonText.FontSize; }
            set { buttonText.FontSize = value; }
        }

		public Brush ButtonBackground
		{
			get { return buttonBorder.Background; }
			set
			{ 
				buttonBorder.Background = value; 
			}
		}
		
		public Brush ButtonBorder
		{
			get { return buttonBorder.BorderBrush; }
			set
			{ 
				buttonBorder.BorderBrush = value; 
			}
		}

        public double ButtonHeight
        {
            get { return buttonBorder.Height; }
            set
            {
                buttonBorder.Height = value;
            }
        }

        public Double ButtonWidth
        {
            get { return buttonBorder.Width; }
            set
            {
                buttonBorder.Width = value;
            }
        }
	}
}